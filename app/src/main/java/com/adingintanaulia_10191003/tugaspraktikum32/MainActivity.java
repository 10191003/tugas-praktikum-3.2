package com.adingintanaulia_10191003.tugaspraktikum32;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button btn_interface, btn_simpan;
    EditText et_data;
    TextView tv_data;
    String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_interface = findViewById(R.id.btn_interface);
        btn_simpan = findViewById(R.id.btn_simpan);
        et_data = findViewById(R.id.et_data);
        tv_data = findViewById(R.id.tv_data);

        SharedPreferences sharedPreferences = getSharedPreferences("TP3", MODE_PRIVATE);
        data = sharedPreferences.getString("data", null);
        tv_data.setText("Output Data : " + data);

        btn_interface.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        btn_simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getSharedPreferences("TP3", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();

                editor.putString("data", et_data.getText().toString());
                editor.apply();

                data = sharedPreferences.getString("data", null);
                tv_data.setText("Output Data : " + data);
            }
        });


    }
}
